#!/bin/bash
source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh

conda activate snakemake-6.9.1

mkdir -p LOGS/snakemake/ LOGS/err/ LOGS/out/

snakemake \
--snakefile $1 \
--jobscript ../workflow_metagenomics/jobscript.sh \
--cluster-config ../workflow_metagenomics/cluster.json \
--cluster "qsub -V -cwd -R y -N {rule} -o {cluster.out} -e {cluster.err} -q {cluster.queue} -pe thread {threads} {cluster.cluster}" \
--keep-going \
--jobs 80 \
--wait-for-files \
--latency-wait 150 \
--restart-times 5 \
--verbose \
--printshellcmds
