rule partial_genes: #fonctionne uniquement avec prodigal
	input:
		"work/prodigal/{sample}/{sample}_prodigal.ffn"
	output:
		"work/prodigal/{sample}/{sample}_partial_{x}.ffn"
	threads:
		2
	shell:
		"conda activate seqkit-2.0.0 "
		"&& "
		"seqkit "
		"grep "
		"--use-regexp "
		"--by-name "
		"--pattern ';partial={wildcards.x};' "
		"--threads {threads} "
		"--out-file {output} "
		"{input} "
		"&& "
		"conda deactivate "

rule pool:
	input:
		expand("work/prodigal/{sample}/{sample}_partial_{{x}}.ffn", sample=config["SAMPLES"])
	output:
		temp("work/prodigal/pool/partial_{x}.ffn")
	shell:
		"cat "
		"{input} "
		"> "
		"{output} "

rule pool_incomplet:
	input:
		expand("work/cdhit/non_redundant_parial_{x}.ffn", x=["01", "10"])
	output:
		temp("work/cdhit/non_redundant_parial_10-01.ffn")
	shell:
		"cat "
		"{input} "
		"> "
		"{output} "

rule cd_hit:
	input:
		"work/prodigal/pool/partial_{x}.ffn"
	output:
		ffn = "work/cdhit/non_redundant_parial_{x}.ffn",
		clustr = temp("work/cdhit/non_redundant_parial_{x}.ffn.clstr")
	threads:
		8
	shell:
		"conda activate cd-hit-4.8.1 "
		"&& "
		"cd-hit-est "
		"-i {input} "
		"-o {output.ffn} "
		"-c 0.95 "
		"-g 1 "
		"-aS 0.90 "
		"-d 0 "
		"-M 0 "
		"-T {threads} "
		"&& "
		"conda deactivate "

rule cd_hit_2D:
	input:
		complete = "work/cdhit/non_redundant_parial_00.ffn",
		incomplete = "work/cdhit/non_redundant_parial_10-01.ffn"
	output:
		ffn = "work/cdhit/catalogue.ffn",
		clustr = temp("work/cdhit/catalogue.ffn.clstr")
	threads:
		8
	shell:
		"conda activate cd-hit-4.8.1 "
		"&& "
		"cd-hit-est-2d "
		"-i {input.complete} "
		"-i2 {input.incomplete} "
		"-o {output.ffn} "
		"-c 0.95 "
		"-aS 0.90 "
		"-g 1 "
		"-d 0 "
		"-M 0 "
		"-T {threads} "
		"&& "
		"conda deactivate "

rule faa_catalogue:
	input:
		ffn = "work/cdhit/catalogue.ffn",
		faa = expand("work/prodigal/{sample}/{sample}_prodigal.faa", sample=config["SAMPLES"])
	output:
		list = temp("work/cdhit/catalogue.list"),
		faa = "work/cdhit/catalogue.faa"
	threads:
		2
	shell:
		"grep '>' {input.ffn} > {output.list} "
		"&& "
		"sed --in-place 's/>//' {output.list} "
		"&& "
		"conda activate seqkit-2.0.0 "
		"&& "
		"seqkit "
		"grep "
		"--by-name "
		"--pattern-file {output.list} "
		"--threads {threads} "
		"--out-file {output.faa} "
		"{input.faa} "
		"&& "
		"conda deactivate "
