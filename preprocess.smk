rule fastp:
	input:
		R1 = "DATA/raw/{reads}_R1.fastq.gz",
		R2 = "DATA/raw/{reads}_R2.fastq.gz"
	output:
		R1 = "DATA/trim/{reads}_R1.fastq.gz",
		R2 = "DATA/trim/{reads}_R2.fastq.gz",
		html = "work/fastp/{reads}_fastp.html",
		json = "work/fastp/{reads}_fastp.json"
	threads:
		4
	shell:
		"conda activate fastp-0.23.1 "
		"&& "
		"fastp "
		"--in1 {input.R1} "
		"--in2 {input.R2} "
		"--out1 {output.R1} "
		"--out2 {output.R2} "
		"--verbose "
		"--length_required 50 "
		"--html {output.html} "
		"--json {output.json} "
		"--report_title \"fastp {wildcards.reads} report\" "
		"--thread {threads} "
		"&& "
		"conda deactivate "

rule interleave:
	input:
		R1 = "DATA/trim/{reads}_R1.fastq.gz",
		R2 = "DATA/trim/{reads}_R2.fastq.gz"
	output:
		R1R2 = "DATA/trim/{reads}_R1R2.fastq.gz"
	shell:
		"conda activate khmer-3.0.0a3 "
		"&& "
		"interleave-reads.py "
		"--output {output.R1R2} "
		"--gzip "
		"{input.R1} "
		"{input.R2} "
		"&& "
		"conda deactivate "

rule sortmerna:
	input:
		R1 = "DATA/trim/{reads}_R1.fastq.gz",
		R2 = "DATA/trim/{reads}_R2.fastq.gz"
	output:
		log = "work/sortmerna/{reads}_rRNA.log",
		R1_rRNA = "work/sortmerna/{reads}_rRNA_fwd.fq.gz",
		R2_rRNA = "work/sortmerna/{reads}_rRNA_rev.fq.gz",
		R1_mRNA = "work/sortmerna/{reads}_mRNA_fwd.fq.gz",
		R2_mRNA = "work/sortmerna/{reads}_mRNA_rev.fq.gz"
	params:
		rRNA = "work/sortmerna/{reads}_rRNA",
		mRNA = "work/sortmerna/{reads}_mRNA"
	threads:
		8
	shell:
		"conda activate sortmerna-4.3.4 "
		"&& "
		"sortmerna "
		"--ref /db/outils/sortmerna/rfam-5.8s-database-id98.fasta "
		"--ref /db/outils/sortmerna/rfam-5s-database-id98.fasta "
		"--ref /db/outils/sortmerna/silva-arc-16s-id95.fasta "
		"--ref /db/outils/sortmerna/silva-arc-23s-id98.fasta "
		"--ref /db/outils/sortmerna/silva-bac-16s-id90.fasta "
		"--ref /db/outils/sortmerna/silva-bac-23s-id98.fasta "
		"--ref /db/outils/sortmerna/silva-euk-18s-id95.fasta "
		"--ref /db/outils/sortmerna/silva-euk-28s-id98.fasta "
		"--reads {input.R1} "
		"--reads {input.R2} "
		"--threads {threads} "
		"--workdir SORTMERNA/{wildcards.reads} "
		"--fastx "
		"--out2 "
		"--aligned {params.rRNA} "
		"--other {params.mRNA} "
		"-v "
		"--paired_in "
		" --zip-out "
		"--num_alignments 1 "
		"--idx-dir /db/outils/sortmerna/idx/ "
		"&& "
		"conda deactivate "
