rule FragGeneScan:
	input:
		unpack(contigs_input)
	output:
		faa = "work/FGS/{sample}/{sample}_FGS.faa",
		ffn = "work/FGS/{sample}/{sample}_FGS.ffn",
		gff = "work/FGS/{sample}/{sample}_FGS.gff",
		out = temp("work/FGS/{sample}/{sample}_FGS.out")
	threads:
		4
	params:
		output = lambda wildcards, output: os.path.splittext(str(output))[0]
	shell:
		"conda activate fraggenescan-1.31 "
		"&& "
		"run_FragGeneScan.pl "
		"-genome={input} "
		"-out={params.output} "
		"-complete=0 " #negative values with complete=1
		"-train=complete "
		"-thread={threads} "
		"&& "
		"sed --in-place 's/*/N/' {output.faa} "
		"&& "
		"conda deactivate "

rule prodigal:
	input:
		unpack(contigs_input)
	output:
		faa = "work/prodigal/{sample}/{sample}_prodigal.faa",
		ffn = "work/prodigal/{sample}/{sample}_prodigal.ffn",
		gff = "work/prodigal/{sample}/{sample}_prodigal.gff"
	shell:
		"conda activate prodigal-2.6.3 "
		"&& "
		"prodigal "
		"-f gff "
		"-i {input} "
		"-a {output.faa} "
		"-d {output.ffn} "
		"-o {output.gff} "
		"-g 11 "
		"-p meta "
		"&& "
		"sed --in-place 's/*$//' {output.faa} "
		"&& "
		"conda deactivate "

def faa_input(wildcards):
	if wildcards.sample == "catalogue":
		return {"faa": "work/cdhit/catalogue.faa"}
	else :
		if config["PROTEINS-PREDICTOR"] == "FragGeneScan":
			return {"faa": "work/FGS/{wildcards.sample}/{wildcards.sample}_FGS.faa".format(wildcards=wildcards)}
		elif config["PROTEINS-PREDICTOR"] == "prodigal":
			return {"faa": "work/prodigal/{wildcards.sample}/{wildcards.sample}_prodigal.faa".format(wildcards=wildcards)}

def ffn_input(wildcards):
	if config["PROTEINS-PREDICTOR"] == "FragGeneScan":
		return {"fnn": "work/FGS/{wildcards.sample}/{wildcards.sample}_FGS.ffn".format(wildcards=wildcards)}
	elif config["PROTEINS-PREDICTOR"] == "prodigal":
		return {"fnn": "work/prodigal/{wildcards.sample}/{wildcards.sample}_prodigal.ffn".format(wildcards=wildcards)}

def gff_input(wildcards):
	if config["PROTEINS-PREDICTOR"] == "FragGeneScan":
		return {"gff": "work/FGS/{wildcards.sample}/{wildcards.sample}_FGS.gff".format(wildcards=wildcards)}
	elif config["PROTEINS-PREDICTOR"] == "prodigal":
		return {"gff": "work/prodigal/{wildcards.sample}/{wildcards.sample}_prodigal.gff".format(wildcards=wildcards)}

def db_input(wildcards):
	if wildcards.db == "nr":
		return {"db": "/db/outils/diamond-0.9.26/nr.dmnd"}
	elif wildcards.db == "swissprot":
		return {"db": "/db/outils/diamond-0.9.26/uniprot.dmnd"}
	elif wildcards.db == "phrogs":
		return {"db": "/home/cmidoux/save/PhROGs/phrogs.dmnd"}
	elif wildcards.db == "RefSeq_Viral":
		return {"db": "/db/outils/diamond-0.9.26/RefSeq_Viral.dmnd"}

rule diamond:
	input:
		unpack(faa_input),
		unpack(db_input)
	output:
		daa = "work/DIAMOND/{db}_{sample}.daa",
		unaligned = "work/DIAMOND/{db}_{sample}_unaligned.faa"
	params:
		tmp = "tmp/diamond/{db}_{sample}/"
	threads:
		20
	shell:
		"mkdir -p {params.tmp} "
		"&& "
		"conda activate diamond-2.0.14 "
		"&& "
		"diamond "
		"blastp "
		"--db {input.db} "
		"--query {input.faa} "
		"--sensitive "
		"--max-target-seqs 1 "
		"--threads {threads} "
		"--out {output.daa} "
		"--outfmt 100 "
		"--salltitles "
		"--sallseqid "
		"--un {output.unaligned} "
		"--verbose "
		"--tmpdir {params.tmp} "
		"&& "
		"conda deactivate "

rule diamondView:
	input:
		daa = "work/DIAMOND/{db}_{sample}.daa"
	output:
		tsv = "report/diamond_{db}_{sample}.tsv"
	params:
		keywords = "qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore stitle"
	shell:
		"conda activate diamond-2.0.14 "
		"&& "
		"diamond "
		"view "
		"--daa {input.daa} "
		"--outfmt 6 {params.keywords} "
		"--out {output.tsv} "
		"--header "
		"&& "
		"conda deactivate "

rule prokka:
	input:
		unpack(contigs_input)
	output:
		log = "work/prokka/{sample}_prokka.log",
		fna = "work/prokka/{sample}_prokka.fna",
		gff = "work/prokka/{sample}_prokka.gff",
		faa = "work/prokka/{sample}_prokka.faa",
		ffn = "work/prokka/{sample}_prokka.ffn",
		tbl = "work/prokka/{sample}_prokka.tbl",
		fsa = "work/prokka/{sample}_prokka.fsa",
		tsv = "work/prokka/{sample}_prokka.tsv",
		txt = "work/prokka/{sample}_prokka.txt",
		gbk = "work/prokka/{sample}_prokka.gbk",
		sqn = "work/prokka/{sample}_prokka.sqn",
		err = "work/prokka/{sample}_prokka.err"
	params:
		output = lambda wildcards, output: os.path.dirname(str(output.gff)),
	threads:
		8
	shell:
		"conda activate prokka-1.14.6 "
		"&& "
		"prokka "
		"--outdir {params.output} "
		"--force "
		"--prefix {wildcards.sample}_prokka "
		"--gffver 3 "
		"--metagenome "
		"--cpus {threads} "
		"--compliant "
		"{input} "
		"&& "
		"conda deactivate "

rule interproscan:
	input:
		unpack(faa_input)
	output:
		"work/interproscan/{sample}/{sample}.{format}"
	threads:
		8
	shell:
		"conda activate interproscan-5.52_86.0 "
		"&& "
		"interproscan.sh "
		"--cpu {threads} "
		"--formats {wildcards.format} "
		"--goterms "
		"--highmem "
		"--input {input.faa} "
		"--iprlookup "
		"--outfile {output} "
		"--pathways "
		"--seqtype p "
		"&& "
		"conda deactivate "
