rule viromeQC:
	input:
		R1 = "DATA/trim/{sample}_R1.fastq.gz",
		R2 = "DATA/trim/{sample}_R2.fastq.gz"
	output:
		"report/viromeQC-{sample}.txt"
	params:
		tmp = "tmp/viromeqc/{sample}/"
	threads:
		8
	shell:
		"mkdir -p {params.tmp} "
		"&& "
		"conda activate viromeqc-1.0 "
		"&& "
		"viromeQC.py "
		"--input {input.R1} {input.R2} "
		"--output {output} "
		"--bowtie2_threads {threads} "
		"--diamond_threads {threads} "
		"--enrichment_preset environmental "
		"--sample_name {wildcards.sample} "
		"--tempdir {params.tmp} "
		"&& "
		"conda deactivate "

rule crispr:
	input:
		unpack(contigs_input)
	output:
		"work/blast/{db}_{sample}.tsv"
	params:
		db = "DATA/blast/{db}", # wget https://crisprcas.i2bc.paris-saclay.fr/Home/DownloadFile?filename=spacer_34.zip -O DATA/spacer.zip && unzip DATA/spacer.zip -d DATA && rm DATA/spacer.zip && conda activate blast-2.9.0 && makeblastdb -in DATA/20210121_spacer_34.fasta -dbtype nucl -out DATA/blast/spacer && rm DATA/20210121_spacer_34.fasta
		header = "qaccver qlen qseq saccver slen sseq pident mismatch gapopen evalue bitscore qcovs length qstart qend sstart send"
	threads:
		20
	shell:
		"conda activate blast-2.12.0 "
		"&& "
		"blastn "
		"-task blastn-short "
		"-db {params.db} "
		"-query {input} "
		"-out {output} "
		"-outfmt '6 {params.header}' "
		"-evalue 0.003 "
		"-word_size 6 "
		"-gapopen 10 "
		"-gapextend 2 "
		"-penalty -1 "
		"-max_target_seqs 1000 "
		"&& "
		"conda deactivate "

rule vibrant:
	input:
		unpack(contigs_input)
	output:
		log = "work/vibrant/VIBRANT_{sample}.contigs/VIBRANT_log_run_{sample}.contigs.log"
	params:
		output = "work/vibrant"
	threads:
		8
	shell:
		"conda activate vibrant-1.2.1 "
		"&& "
		"VIBRANT_run.py "
		"-i {input} "
		"-f nucl "
		"-folder {params.output} "
		"-t {threads} "
		" -virome "
		"&& "
		"conda deactivate"

rule checkv:
	input:
		unpack(contigs_input)
	output: 
		"work/checkv/{sample}/quality_summary.tsv"
	threads:
		8
	params:
		db = "/db/outils/checkv-0.7.0",
		output = lambda wildcards, output: os.path.dirname(str(output))
	shell:
		"conda activate checkv-0.8.1 "
		"&& "
		"checkv end_to_end "
		"-d {params.db} "
		"-t {threads} "
		"{input} "
		"{params.output} "
		"&& "
		"conda deactivate "

# rule virsorterDB:
# 	output: 
# 		done = "DATA/virsorter/Done_all_setup"
# 	threads:
# 		8
# 	params:
# 		output = lambda wildcards, output: os.path.dirname(str(output.done))
# 	shell:
# 		"conda activate virsorter2-2.2.3 "
# 		"&& "
# 		"virsorter setup "
# 		"-d {params.output} "
# 		"-j {threads} "
# 		"&& "
# 		"conda deactivate "
# 
# rule virsorter2:
# 	input:
# 		contigs = unpack(contigs_input),
# 		db = "DATA/virsorter/Done_all_setup" #conda activate virsorter2-2.2.3 && virsorter setup -d DATA/virsorter -j 4 && conda deactivate
# 	output:
# 		"work/virsorter/{sample}/final-viral-combined.fa"
# 	params:
# 		db = lambda wildcards, input: os.path.dirname(str(input.db)),
# 		output = lambda wildcards, output: os.path.dirname(str(output))
# 	threads:
# 		8
# 	shell:
# 		"conda activate virsorter2-2.2.3 "
# 		"&& "
# 		"virsorter run "
# 		"--working-dir {params.output} "
# 		"--db-dir {params.db} "
# 		"--seqfile {input.contigs} "
# 		"--jobs {threads} "
# 		"--min-length 1500 "
# 		"all "
# 		"&& "
# 		"conda deactivate "

rule CAT:
	input:
		contigs = contigs_input
#		unpack(faa_input)
	output:
		taxnames = "work/CAT/{sample}/{sample}.TaxNames.txt"
	params:
		db = "/db/outils/CAT-5.2.3/CAT_prepare_20210107",
		output = lambda wildcards, output: os.path.dirname(str(output))
	threads:
		20
	shell:
		"conda activate cat-5.2.3 "
		"&& "
		"CAT contigs "
		"--contigs_fasta {input.contigs} "
		"--database_folder {params.db}/2021-01-07_CAT_database/ "
		"--taxonomy_folder {params.db}/2021-01-07_taxonomy/ "
		"--out_prefix {params.output}/{wildcards.sample} "
#		"--proteins_fasta {input.faa} "
		"--path_to_diamond {params.db}/Diamond_2.0.6/diamond "
		"--nproc {threads} "
		"&& "
		"CAT add_names "
		"--input_file {params.output}/{wildcards.sample}.contig2classification.txt "
		"--output_file {output.taxnames} "
		"--taxonomy_folder {params.db}/2021-01-07_taxonomy/ "
		"--only_official "
		"--exclude_scores "
		"&& "
		"conda deactivate "

rule refinem:
	input:
		contigs = contigs_input,
		bin = directory("work/metabat/{sample}/bin"),
		bam = "work/bowtie/align-{sample}-{sample}.bam"
	output:
		done = "work/refinem/{sample}/done"
	params:
		output = lambda wildcards, output: os.path.dirname(str(output))
	threads:
		20
	shell:
		"conda activate refinem-0.1.2 "
		"&& "
		"refinem scaffold_stats "
		"--genome_ext fa "
		"--cpus {threads} "
		"{input.contigs} "
		"{input.bin} "
		"{params.output}/stats/ "
		"{input.bam} "
		"&& "
		"refinem outliers "
		"{params.output}/stats/scaffold_stats.tsv "
		"{params.output}/outliers/ "
		"&& "
		"refinem filter_bins "
		"--genome_ext fa "
		"{input.bin} "
		"{params.output}/outliers/outliers.tsv "
		"{params.output}/filtered_outliers/ "
		"&& "
		"refinem call_genes "
		"--genome_ext fa "
		"--cpus {threads} "
		"{input.bin} "
		"{params.output}/gene/ "
		"&& "
		"refinem taxon_profile "
		"--cpus {threads} "
		"{params.output}/gene/ " 
		"{params.output}/stats/scaffold_stats.tsv "
		"/db/outils/RefineM/diamond/gtdb_r95_protein_db.2020-07-30.faa.dmnd " #<reference_db> (Database/pro_db.dmnd)
		"/db/outils/RefineM/flat/gtdb_r95_taxonomy.2020-07-30.tsv " #<reference_taxonomy> (Taxon/gtdb_r95_taxonomy.2020-07-30.tsv)
		"{params.output}/taxon_profile/ "
		"&& "
		"refinem taxon_filter "
		"--cpus {threads} "
		"{params.output}/taxon_profile/ "
		"{params.output}/taxon_filter.tsv "
		"&& "
		"refinem filter_bins "
		"--genome_ext fa "
		"{input.bin} "
		"{params.output}/taxon_filter.tsv "
		"{params.output}/filtered_taxon/ "
		"&& "
		"conda deactivate "
		"&& "
		"touch {output.done} "
