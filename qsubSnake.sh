mkdir -p LOGS/snakemake
qsub -V -cwd -o LOGS/snakemake/ -e LOGS/snakemake/ -q long.q -N `basename $1` ../workflow_metagenomics/RunSnake.sh $1
