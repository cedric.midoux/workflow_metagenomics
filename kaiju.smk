def kaiju_input(wildcards):
	if wildcards.focus == "contigs":
		return contigs_input(wildcards)
	elif wildcards.focus == "genes":
		return ffn_input(wildcards)
	elif wildcards.focus == "reads":
		return {"R1": "DATA/trim/{wildcards.sample}_R1.fastq.gz".format(wildcards=wildcards), "R2": "DATA/trim/{wildcards.sample}_R2.fastq.gz".format(wildcards=wildcards)}

rule kaiju:
	input:
		unpack(kaiju_input)
	output:
		"work/kaiju/{focus,[a-z]+}_{sample}.kaijuNR"
	threads:
		64
	params:
		input = lambda wildcards, input: "-i %s -j %s"%(input.R1, input.R2) if wildcards.focus == "reads" else "-i %s"%input
	shell:
		"conda activate kaiju-1.8.1 "
		"&& "
		"kaiju "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-f /db/outils/kaiju-2021-03/nr_euk/kaiju_db_nr_euk.fmi "
		"{params.input} "
		"-o {output} "
		"-z {threads} "
		"&& "
		"conda deactivate "

rule krona:
	input:
		"work/kaiju/{sample}.kaijuNR"
	output:
		temp("work/kaiju/{sample}.krona")
	shell:
		"conda activate kaiju-1.8.1 "
		"&& "
		"kaiju2krona "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-n /db/outils/kaiju-2021-03/nr_euk/names.dmp "
		"-i {input} "
		"-o {output} "
		"-u "
		"&& "
		"conda deactivate "

rule kronaHTML:
	input:
		"work/kaiju/{sample}.krona"
	output:
		"report/{sample}-krona.html"
	shell:
		"conda activate krona-2.8 "
		"&& "
		"ktImportText "
		"-o {output} "
		"{input} "
		"&& "
		"conda deactivate "

rule kronaHTMLall:
	input:
		expand("work/kaiju/{{focus}}_{sample}.krona", sample=config["SAMPLES"])
	output:
		"report/{focus}_krona_all.html"
	shell:
		"conda activate krona-2.8 "
		"&& "
		"ktImportText "
		"-o {output} "
		"{input} "
		"&& "
		"conda deactivate "

rule kronaNames:
	input:
		"work/kaiju/{sample}.kaijuNR"
	output:
		"report/{sample}-taxNames.tsv"
	shell:
		"conda activate kaiju-1.8.1 "
		"&& "
		"kaiju-addTaxonNames "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-n /db/outils/kaiju-2021-03/nr_euk/names.dmp "
		"-i {input} "
		"-o {output} "
		"-r superkingdom,phylum,order,class,family,genus,species "
		"&& "
		"conda deactivate "
