shell.executable("/bin/bash")
shell.prefix("source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh; ")

import os
configfile: "./config.json"

rule all:
	input:
		#QC
		"report/multiqc_report.html",
		"report/reads_krona_all.html",

		#assembly
		"report/quast_results/report.html",
		##krona
		expand("report/contigs_{sample}-krona.html", sample=config["SAMPLES"]),
		expand("report/contigs_{sample}-taxNames.tsv", sample=config["SAMPLES"]),
		expand("report/genes_{sample}-krona.html", sample=config["SAMPLES"]),
		expand("report/genes_{sample}-taxNames.tsv", sample=config["SAMPLES"]),
		##count
		expand("report/count-contigs-{sample}-{sample}.tsv", sample=config["SAMPLES"]),
		expand("report/count-genes-{sample}-{sample}.tsv", sample=config["SAMPLES"]),
		##annot
		expand("report/diamond_{db}_{sample}.tsv", sample=config["SAMPLES"], db=["nr", "swissprot"]),
		expand("work/interproscan/{sample}/{sample}.tsv", sample=config["SAMPLES"]),
		expand("work/prokka/{sample}_prokka.txt", sample=config["SAMPLES"]),
		##binning
		expand("work/metabat/{sample}/kaiju.done", sample=config["SAMPLES"]),
		expand("work/checkm/{sample}/lineage.ms", sample=config["SAMPLES"]),

		#coassembly
		"report/quast_coassembly/report.html",
		##krona
		"report/contigs_coassembly-krona.html",
		"report/contigs_coassembly-taxNames.tsv",
		"report/genes_coassembly-taxNames.tsv",
		##count
		expand("report/count-contigs-coassembly-{sample}.tsv", sample=config["SAMPLES"]),
		expand("report/count-genes-coassembly-{sample}.tsv", sample=config["SAMPLES"]),
		##binning
		"work/metabat/coassembly/kaiju.done",
		"work/checkm/coassembly/lineage.ms",

		#catalogue
		##count
		expand("report/count-contigs-catalogue-{sample}.tsv", sample=config["SAMPLES"]),
		##krona
		"report/contigs_catalogue-krona.html",
		"report/contigs_catalogue-taxNames.tsv",
		##annot
		"report/diamond_nr_catalogue.tsv",

		#add-on
		expand("work/addon/{sample}.small_contigs.taxNames.tsv", sample=config["SAMPLES"]),
		expand("work/addon/{sample}-{sample}.unmapped.taxNames.tsv", sample=config["SAMPLES"]),
		expand("work/addon/catalogue-{sample}.unmapped.taxNames.tsv", sample=config["SAMPLES"]),

		#virome
		expand("report/viromeQC-{sample}.txt", sample=config["SAMPLES"]),
		expand("work/blast/spacer_{sample}.tsv", sample=config["SAMPLES"]),
		expand("work/vibrant/VIBRANT_{sample}.contigs/VIBRANT_log_run_{sample}.contigs.log", sample=config["SAMPLES"]),
		expand("work/checkv/{sample}/quality_summary.tsv", sample=config["SAMPLES"]),
		expand("work/CAT/{sample}/{sample}.TaxNames.txt", sample=config["SAMPLES"]),
		expand("work/refinem/{sample}/done", sample=config["SAMPLES"]),


include: "../workflow_metagenomics/quality.smk"
include: "../workflow_metagenomics/preprocess.smk"
include: "../workflow_metagenomics/kaiju.smk"
include: "../workflow_metagenomics/assembly.smk"
include: "../workflow_metagenomics/annotation.smk"
include: "../workflow_metagenomics/count.smk"
include: "../workflow_metagenomics/catalogue.smk"
include: "../workflow_metagenomics/binning.smk"
include: "../workflow_metagenomics/add-on.smk"
include: "../workflow_metagenomics/virome.smk"
