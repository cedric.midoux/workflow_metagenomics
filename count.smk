rule index :
	input:
		unpack(contigs_input)
	output:
		expand("work/index/{{sample}}.{ext}.bt2l", ext=["1", "2", "3", "4", "rev.1", "rev.2"])
	params:
		index = "work/index/{sample}"
	shell:
		"conda activate bowtie2-2.4.4 "
		"&& "
		"bowtie2-build "
		"--large-index "
		"{input} "
		"{params.index} "
		"&& "
		"conda deactivate "


rule mapping:
	input:
		R1 = "DATA/trim/{reads}_R1.fastq.gz",
		R2 = "DATA/trim/{reads}_R2.fastq.gz",
		i = expand("work/index/{{sample}}.{ext}.bt2l", ext=["1", "2", "3", "4", "rev.1", "rev.2"])
	output:
		bam = "work/bowtie/align-{sample}-{reads}.bam",
		bai = "work/bowtie/align-{sample}-{reads}.bam.bai"
	params:
		index = "work/index/{sample}"
	threads:
		8
	shell:
		"conda activate bowtie2-2.4.4 "
		"&& "
		"conda activate --stack samtools-1.14 "		
		"&& "
		"bowtie2 "
		"--threads {threads} "
		"-x {params.index} "
		"-1 {input.R1} "
		"-2 {input.R2} "
		"|"
		"samtools view "
		"-h "
		"-b "
		"|"
		"samtools sort "
		"--threads {threads} "
		"--output-fmt BAM "
		"-o {output.bam} "
		"- "
		"&& "
		"samtools index "
		"{output.bam} "
		"&& "
		"conda deactivate "
		"&& "
		"conda deactivate "

rule count_contig:
	input:
		bam = "work/bowtie/align-{sample}-{reads}.bam",
		bai = "work/bowtie/align-{sample}-{reads}.bam.bai"
	output:
		"report/count-contigs-{sample}-{reads}.tsv"
	shell:
		"conda activate samtools-1.14 "
		"&& "
		"samtools idxstats "
		"{input.bam} "
		"> "
		"{output}"
		"&& "
		"conda deactivate "

rule count_gene:
	input:
		unpack(gff_input),
		bam = "work/bowtie/align-{sample}-{reads}.bam",
		bai = "work/bowtie/align-{sample}-{reads}.bam.bai"
	output:
		"report/count-genes-{sample}-{reads}.tsv"
	shell:
		"conda activate htseq-0.13.5 "
		"&& "
		"htseq-count "
		"--format bam "
		"--stranded no "
		"--minaqual 10 "
		"--type CDS "
		"--idattr ID "
		"--mode union "
		"--nonunique none "
		"--order pos "
		#"--samout {output} "
		"{input.bam} "
		"{input.gff} "
		"> "
		"{output} "
		"&& "
		"conda deactivate "
