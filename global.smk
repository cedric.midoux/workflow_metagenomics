shell.executable("/bin/bash")
shell.prefix("source ~/.bashrc; ")

import os
configfile: "./config.json"

rule all:
	input:
		"report/multiqc_report.html",
		expand("report/reads_{sample}-krona.html", sample=config["SAMPLES"]),
		#coassembly
		"report/quast_coassembly/report.html",
		expand("report/count-contigs-coassembly-{sample}.tsv", sample=config["SAMPLES"]),
		expand("report/count-genes-coassembly-{sample}.tsv", sample=config["SAMPLES"]),
		"report/diamond_nr_coassembly.tsv",
		"report/diamond_swissprot_coassembly.tsv",
		"report/contigs_coassembly-taxNames.tsv",
		"report/genes_coassembly-taxNames.tsv",
		"work/checkm/coassembly/lineage.ms",
		#catalogue
		"report/quast_results/report.html",
		expand("report/count-contigs-catalogue-{sample}.tsv", sample=config["SAMPLES"]),
		"report/diamond_nr_catalogue.tsv",
		"report/contigs_catalogue-taxNames.tsv",
		#add-on
		expand("work/addon/{sample}.small_contigs.taxNames.tsv", sample=config["SAMPLES"]),
		#expand("work/addon/catalogue-{sample}.unmapped.taxNames.tsv", sample=config["SAMPLES"]),
		#virome
		expand("report/viromeQC-{sample}.txt", sample=config["SAMPLES"]),
		expand("work/virhostmatcher/{sample}/done", sample=config["SAMPLES"]),
		expand("work/blast/spacer_{sample}.tsv", sample=config["SAMPLES"]),


include: "../workflow_metagenomics/quality.smk"
include: "../workflow_metagenomics/preprocess.smk"
include: "../workflow_metagenomics/kaiju.smk"
include: "../workflow_metagenomics/assembly.smk"
include: "../workflow_metagenomics/annotation.smk"
include: "../workflow_metagenomics/count.smk"
include: "../workflow_metagenomics/catalogue.smk"
include: "../workflow_metagenomics/binning.smk"
include: "../workflow_metagenomics/add-on.smk"
include: "../workflow_metagenomics/virome.smk"
