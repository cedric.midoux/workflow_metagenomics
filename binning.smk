rule jgi:
	input:
		bam = lambda wildcards: expand("work/bowtie/align-coassembly-{reads}.bam", reads=config["SAMPLES"]) if wildcards.sample=="coassembly" else "work/bowtie/align-{sample}-{sample}.bam"
	output:
		"work/metabat/abd/{sample}.txt"
	shell:
		"conda activate metabat2-2.15 "
		"&& "
		"jgi_summarize_bam_contig_depths "
		"--outputDepth {output} "
		"{input.bam} "
		"&& "
		"conda deactivate "

checkpoint metabat:
	input:
		adb = "work/metabat/abd/{sample}.txt",
		contigs = contigs_input
	output:
		clusters = directory("work/metabat/{sample}/bin")
	params:
		clustrsize = 200000
	threads:
		8
	shell:
		"conda activate metabat2-2.15 "
		"&& "
		"metabat2 "
		"--inFile {input.contigs} "
		"--outFile {output}/{wildcards.sample} "
		"--minContig 5000 "
		"--minClsSize {params.clustrsize} "
		"--numThreads {threads} "
		"--unbinned "
		"--abdFile {input.adb} "
		"--verbose "
		"&& "
		"conda deactivate "
		"&& "
		"find "
		"{output} "
		"-empty "
		"-name {wildcards.sample}.*.fa "
		"-delete"

rule metabat_kaiju:
	input:
		clustr = "work/metabat/{sample}/bin/{sample}.{num}.fa",
		kaiju = "report/contigs_{sample}-taxNames.tsv"
	output:
		"work/metabat/{sample}/kaiju/{sample}.{num}_kaiju.tsv"
	shell:
		"conda activate seqkit-2.0.0 "
		"&& "
		"seqkit seq "
		"--name "
		"{input.clustr} "
		" | "
		"grep "
		"--no-messages "
		"--word-regexp "
		"--file - "
		"{input.kaiju} "
		"> "
		"{output} "
		"&& "
		"conda deactivate "

def aggregate_metabat_kaiju(wildcards):
	checkpoint_output = checkpoints.metabat.get(**wildcards).output[0]
	return expand("work/metabat/{sample}/kaiju/{sample}.{i}_kaiju.tsv",
		sample=wildcards.sample,
		i=glob_wildcards(os.path.join(checkpoint_output, "{sample}.{i}.fa")).i)

rule metabat_kaiju_done:
	input:
		aggregate_metabat_kaiju
	output:
		"work/metabat/{sample}/kaiju.done"
	shell:
		"touch {output}"

rule checkm:
	input:
		directory("work/metabat/{sample}/bin")
	output:
		"work/checkm/{sample}/lineage.ms"
	threads:
		4
	params:
		output = lambda wildcards, output: os.path.dirname(str(output))
	shell:
		"conda activate checkm-genome-1.1.3 "
		"&& "
		"checkm "
		"lineage_wf "
		"-t {threads} "
		"-x fa "
		"{input} "
		"{params.output} "
		"&& "
		"conda deactivate "
