rule viromeQC:
	input:
		R1 = "DATA/trim/{sample}_R1.fastq.gz",
		R2 = "DATA/trim/{sample}_R2.fastq.gz"
	output:
		"report/viromeQC-{sample}.txt"
	threads:
		8
	shell:
		"conda activate viromeQC-1.0 "
		"&& "
		"viromeQC.py "
		"--input {input.R1} {input.R2} "
		"--output {output} "
		"--bowtie2_threads {threads} "
		"--diamond_threads {threads} "
		"--enrichment_preset environmental "
		"--sample_name {wildcards.sample} "
		"--tempdir /projet/tmp/ "
		"&& "
		"conda deactivate "

rule virhostmatcher:
	input:
		unpack(contigs_input)
	output:
		done = "work/virhostmatcher/{sample}/done"
	params:
		splittedGenomes = "work/virhostmatcher/{sample}/splittedGenomes",
		output = lambda wildcards, output: os.path.dirname(str(output))
	shell:
		"mkdir -p {params.splittedGenomes} "
		"&& "
		"awk '/^>/ {{if(x>0) {{close(outname); x=0}} match($0, \">([^| ]*)\", record);outname=sprintf(\"{params.splittedGenomes}/%s.fa\",record[1]); if (x>0) {{print >> outname}} else {{print > outname;}} x++; next;}} {{if(x>0) print >> outname;}}' {input} " # https://github.com/soedinglab/WIsH#tricks
		"&& "
		# "conda activate vhm "
		# "&& "
		"vhm.py "
		"--virusFaDir {params.splittedGenomes} "
		"--hostFaDir /projet/irstea/WIsH_2020/Host/ "
		"--out {params.output} "
		"--taxa /projet/irstea/WIsH_2020/Host.txt "
		"--d2star 0 "
		# "&& "
		# "conda deactivate "
		"&& "
		"touch {output.done} "

rule crispr:
	input:
		unpack(contigs_input)
	output:
		"work/blast/{db}_{sample}.tsv"
	params:
		db = "DATA/blast/{db}", # wget https://crisprcas.i2bc.paris-saclay.fr/Home/DownloadFile?filename=20190618_spacer_34.zip -O DATA/spacer.zip && unzip DATA/spacer.zip -d DATA && rm DATA/spacer.zip && makeblastdb -in DATA/20190618_spacer_34.fasta -dbtype nucl -out DATA/blast/spacer && rm DATA/20190618_spacer_34.fasta
		header = "qaccver qlen qseq saccver slen sseq pident mismatch gapopen evalue bitscore qcovs length qstart qend sstart send"
	threads:
		20
	shell:
		"conda activate blast-2.9.0 "
		"&& "
		"blastn "
		"-task blastn-short "
		"-db {params.db} "
		"-query {input} "
		"-out {output} "
		"-outfmt '6 {params.header}' "
		"-evalue 0.003 "
		"-word_size 6 "
		"-gapopen 10 "
		"-gapextend 2 "
		"-penalty -1 "
		"-max_target_seqs 1000 "
		"&& "
		"conda deactivate "
