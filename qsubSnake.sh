mkdir -p ./.log/out/ ./.log/err/
qsub -V -cwd -o ./.log/out/ -e ./.log/err/ -q maiage.q,long.q -N `basename $1` ../workflow_metagenomics/RunSnake.sh $1
