rule select_small:
	input:
		"work/metaSPADES/{reads}/contigs.fasta"
	output:
		contigs = "work/addon/{reads}.small_contigs.fasta"
	params:
		min_len = config["CONTIGS_LEN"]
	shell:
		"conda activate seqkit-2.0.0 "
		"&& "
		"seqkit "
		"seq "
		"--max-len {params.min_len} "
		"--out-file {output.contigs} "
		"{input} "
		"&& "
		"conda deactivate"

rule kaiju_small:
	input:
		"work/addon/{sample}.small_contigs.fasta"
	output:
		"work/addon/{sample}.small_contigs.kaijuNR"
	threads:
		20
	shell:
		"conda activate kaiju-1.8.1 "
		"&& "
		"kaiju "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-f /db/outils/kaiju-2021-03/nr_euk/kaiju_db_nr_euk.fmi "
		"-i {input} "
		"-o {output} "
		"-z {threads} "
		"&& "
		"conda deactivate "


rule select_unmapped:
	input:
		"work/bowtie/align-{sample}-{reads}.bam"
	output:
		bam = temp("work/addon/unmapped-{sample}-{reads}.bam"),
		fastq = "work/addon/unmapped-{sample}-{reads}.fastq"
	shell:
		"conda activate samtools-1.14 "
		"&& "
		"samtools "
		"view "
		"-f 4 "
		"-o {output.bam} "
		"{input} "
		"&& "
		"conda deactivate "
		"&& "
		"conda activate bedtools-2.30.0 "
		"&& "
		"bedtools "
		"bamtofastq "
		"-i {output.bam} "
		"-fq {output.fastq} "
		"&& "
		"conda deactivate "

rule kaiju_unmapped:
	input:
		"work/addon/unmapped-{sample}-{reads}.fastq"
	output:
		"work/addon/{sample}-{reads}.unmapped.kaijuNR"
	threads:
		20
	shell:
		"conda activate kaiju-1.8.1 "
		"&& "
		"kaiju "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-f /db/outils/kaiju-2021-03/nr_euk/kaiju_db_nr_euk.fmi "
		"-i {input} "
		"-o {output} "
		"-z {threads} "
		"&& "
		"conda deactivate "


rule kronaNames_addon:
	input:
		"work/addon/{sample}.{addon}.kaijuNR"
	output:
		"work/addon/{sample}.{addon}.taxNames.tsv"
	shell:
		"conda activate kaiju-1.7.3 "
		"&& "
		"kaiju-addTaxonNames "
		"-t /db/outils/kaiju-2021-03/nr_euk/nodes.dmp "
		"-n /db/outils/kaiju-2021-03/nr_euk/names.dmp "
		"-i {input} "
		"-o {output} "
		"-r superkingdom,phylum,order,class,family,genus,species "
		"&& "
		"conda deactivate"
