def khmer_input(wildcards):
	if config["SORTMERNA"]:
		return {"R1": "work/sortmerna/{wildcards.reads}_mRNA_fwd.fq.gz".format(wildcards=wildcards),
				"R2": "work/sortmerna/{wildcards.reads}_mRNA_rev.fq.gz".format(wildcards=wildcards)}
	else:
		return {"R1R2": "DATA/trim/{wildcards.reads}_R1R2.fastq.gz".format(wildcards=wildcards)}

rule khmer:
	input:
		unpack(khmer_input)
	output:
		R1R2 = "work/khmer/{reads}_R1R2.fastq.gz",
		log = "work/khmer/{reads}.log"
	threads:
		10
	params:
		mem_tot = int(90 * 1e9),
		K = 32,
		C = 100
	shell:
		"conda activate khmer-3.0.0a3 "
		"&& "
		"normalize-by-median.py "
		"--ksize {params.K} "
		"--max-memory-usage {params.mem_tot} "
		"--cutoff {params.C} "
		"--paired "
		"--report {output.log} "
		"--output {output.R1R2} "
		"--gzip "
		"{input.R1R2} "
		"&& "
		"conda deactivate "

def assembly_input(wildcards):
	if wildcards.reads == "coassembly":
		if config["NORMALIZATION"]:
			return {"R1R2": expand("work/khmer/{reads}_R1R2.fastq.gz", reads=config["SAMPLES"])}
		elif config["SORTMERNA"]:
			return {"R1": expand("work/sortmerna/{reads}_mRNA_fwd.fq.gz", reads=config["SAMPLES"]), "R2": expand("work/sortmerna/{reads}_mRNA_rev.fq.gz", reads=config["SAMPLES"])}
		else:
			return {"R1": expand("DATA/trim/{reads}_R1.fastq.gz", reads=config["SAMPLES"]), "R2": expand("DATA/trim/{reads}_R2.fastq.gz", reads=config["SAMPLES"])}

	else:
		if config["NORMALIZATION"]:
			return {"R1R2": ["work/khmer/{wildcards.reads}_R1R2.fastq.gz".format(wildcards=wildcards)]}
		elif config["SORTMERNA"]:
			return {"R1": expand("work/sortmerna/{reads}_mRNA_fwd.fq.gz", reads=config["SAMPLES"]), "R2": expand("work/sortmerna/{reads}_mRNA_rev.fq.gz", reads=config["SAMPLES"])}
		else:
			return {"R1": ["DATA/trim/{wildcards.reads}_R1.fastq.gz".format(wildcards=wildcards)], "R2": ["DATA/trim/{wildcards.reads}_R2.fastq.gz".format(wildcards=wildcards)]}

rule megahit:
	input:
		unpack(assembly_input)
	output:
		contigs = "work/megahit/{reads}/{reads}.contigs.fa",
		done = "work/megahit/{reads}/done"
	threads:
		20
	params:
		min_len = config["CONTIGS_LEN"],
		mem_tot = int(250 * 1e9),
		tmp = "tmp/megahit/{reads}/",
		input = lambda wildcards, input: "-1 %s -2 %s"%(",".join(input.R1), ",".join(input.R2)) if set(['R1', 'R2']).issubset(input.keys()) else "--12 %s"%",".join(input.R1R2),
		output = lambda wildcards, output: os.path.dirname(str(output.contigs))
	shell:
		"mkdir -p {params.tmp} "
		"&& "
		"conda activate megahit-1.2.9 "
		"&& "
		"megahit "
		"{params.input} "
		"--continue "
		"--preset meta-large "
		"--num-cpu-threads {threads} "
		"--memory {params.mem_tot} "
		"--tmp-dir {params.tmp} "
		"--out-dir {params.output} "
		"--out-prefix {wildcards.reads} "
		"--min-contig-len {params.min_len} "
		"--force " #pas dans la doc mais fonctionne
		"--verbose "
		"&& "
		"conda deactivate "

rule metaspades:
	input:
		unpack(assembly_input)
	output:
		contigs = "work/metaSPADES/{reads}/{reads}.contigs.fasta",
		all_contigs = "work/metaSPADES/{reads}/contigs.fasta",
		done = "work/metaSPADES/{reads}/done"
	threads:
		20
	params:
		min_len = config["CONTIGS_LEN"],
		mem_tot = 500,
		tmp = "tmp/metaspades/{reads}/",
		input = lambda wildcards, input: "-1 %s -2 %s"%(" -1 ".join(input.R1), " -2 ".join(input.R2)) if set(['R1', 'R2']).issubset(input.keys()) else "--12 %s"%" --12 ".join(input.R1R2),
		output = lambda wildcards, output: os.path.dirname(str(output.contigs))
	shell:
		"mkdir -p {params.tmp} "
		"&& "
		"conda activate spades-3.15.3 "
		"&& "
		"spades.py "
		"--threads {threads} "
		"--memory {params.mem_tot} "
		"--tmp-dir {params.tmp} "
		"--meta "
		"{params.input} "
		"-o {params.output} "
		"&& "
		"conda deactivate "
		"&& "
		"touch {output.done} "
		"&& "
		"conda activate seqkit-2.0.0 "
		"&& "
		"seqkit "
		"seq "
		"--min-len {params.min_len} "
		"--out-file {output.contigs} "
		"{output.all_contigs} "
		"&& "
		"conda deactivate "

def contigs_input(wildcards):
	if wildcards.sample == "catalogue":
		return ["work/cdhit/catalogue.ffn"]
	else :
		if config["ASSEMBLER"] == "megahit":
			return ["work/megahit/{wildcards.sample}/{wildcards.sample}.contigs.fa".format(wildcards=wildcards)]
		elif config["ASSEMBLER"] == "metaspades":
			return ["work/metaSPADES/{wildcards.sample}/{wildcards.sample}.contigs.fasta".format(wildcards=wildcards)]

def contigsExpand_input(wildcards):
	if config["ASSEMBLER"] == "megahit":
		return [expand("work/megahit/{sample}/{sample}.contigs.fa", sample=config["SAMPLES"])]
	elif config["ASSEMBLER"] == "metaspades":
		return [expand("work/metaSPADES/{sample}/{sample}.contigs.fasta", sample=config["SAMPLES"])]

def coassembly_contigs_input(wildcards):
	if config["ASSEMBLER"] == "megahit":
		return ["work/megahit/coassembly/coassembly.contigs.fa"]
	elif config["ASSEMBLER"] == "metaspades":
		return ["work/metaSPADES/coassembly/coassembly.contigs.fasta"]

rule quast:
	input:
		unpack(contigsExpand_input)
	output:
		"report/quast_results/report.html"
	threads:
		4
	params:
		output = lambda wildcards, output: os.path.dirname(str(output))
	shell:
		"conda activate quast-5.0.2 "
		"&& "
		"metaquast "
		"--mgm "
		"--output-dir {params.output} "
		"-L "
		"--threads {threads} "
		"{input} "
		"&& "
		"conda deactivate "

rule coassembly_quast:
	input:
		unpack(coassembly_contigs_input)
	output:
		"report/quast_coassembly/report.html"
	threads:
		4
	params:
		output = lambda wildcards, output: os.path.dirname(str(output))
	shell:
		"conda activate quast-5.0.2 "
		"&& "
		"quast "
		"--mgm "
		"--output-dir {params.output} "
		"-L "
		"--threads {threads} "
		"{input} "
		"&& "
		"conda deactivate "
