#!/bin/bash
source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh

conda activate snakemake-6.9.1

snakemake \
--snakefile $1 \
--jobscript ../workflow_metagenomics/jobscript.sh \
--cluster-config ../workflow_metagenomics/cluster.json \
--dryrun \
--printshellcmds \
--verbose
