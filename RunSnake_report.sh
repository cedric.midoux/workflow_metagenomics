#!/bin/bash
source /usr/local/genome/Anaconda3/etc/profile.d/conda.sh

conda activate snakemake-6.9.1

mkdir -p report/

snakemake \
--snakefile $1 \
--jobscript ../workflow_metagenomics/jobscript.sh \
--cluster-config ../workflow_metagenomics/cluster.json \
--report report/`basename $1 .smk`_report.html \
--verbose
